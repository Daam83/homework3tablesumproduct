package pl.codementors.Task3;

/**
 * Created by student on 23.05.17.
 */
public class Task3 {


    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 7, 6, 4, 6, 2, 3}; // tworzenie tablicy i przypisanie jej wartości
        int Suma = 0; // deklaracja zmiennej pomocniczej do Sumowania. Wartość początkowa 0.
        int Iloczyn = 1; // deklaracja zmiennej pomocniczej do iloczynu. Wartość początkowa 1.
        for (int i : array) // pętla do tabeli for each.
        {
            System.out.println(i); // wypisuje waszystkie wartości poszczególnych elementóœ tabeli.

        }

        for (int i = 0; i < array.length; i++) // pętla do tabeli
        {
            Iloczyn = Iloczyn * array[i]; // iloczyn poszczególnych części tablicy
            Suma = Suma + array[i]; // suma poszczególnych części tablicy
        }

        System.out.println("Suma wszystkich elementów tablicy"); //
        System.out.println(Suma); // suma  wszystkich elementów tablicy
        System.out.println("Iloczyn wszystkich elementów tablicy");
        System.out.println(Iloczyn); // iloczyn wszystkich elementóœ tablicy
    }
}
